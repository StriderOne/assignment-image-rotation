#include "file.h"

enum open_status open_file(char* filename, FILE** file, char* actionTYPE) {
    FILE *output_image = fopen(filename, actionTYPE);
    *file = output_image;
    
    if (output_image == NULL)
        return OPEN_ERROR;
    // printf("%p", (void *) output_image);
    return OPEN_OK;

}

enum close_status close_file(FILE* file) {
    if (fclose(file) == -1)
        return CLOSE_ERROR;
    return CLOSE_OK;
}
