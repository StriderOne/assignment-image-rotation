#include "image.h"
#include <malloc.h>
#include <math.h>
#include <stdint.h>


struct image create_image(uint64_t width, uint64_t height) {
    struct pixel* image_data = (struct pixel*) malloc(width*height*sizeof(struct pixel));
    for (size_t i = 0; i < width*height; i++) {
        struct pixel pixel = {0};
        image_data[i] = pixel;
    }
    struct image new_image = (struct image) {width, height, image_data};
    return new_image;
}

struct pixel_row create_pixel_row(uint64_t width) {
    struct pixel* row_data = (struct pixel*) malloc(width*sizeof(struct pixel));
    for (size_t i = 0; i < width; i++) {
        struct pixel pixel = {0};
        row_data[i] = pixel;
    }
    struct pixel_row new_row = (struct pixel_row) {width, row_data};
    return new_row;
}

// input: image type of struct image, x - number of row (from 0 to height - 1), y - number of column (from 0 to weight - 1)
struct pixel get_image_pixel(const struct image* image, uint64_t x, uint64_t y) {
    if (x < 0 || x >= (image->height) || y < 0 || y >= (image->width))
        return (struct pixel) {0, 0, 0}; 
    return image->data[x * (image->width) + y];
}

// input: image type of struct image, x - number of row (from 0 to height - 1), y - number of column (from 0 to weight - 1), new pixel - struct pixel
void set_image_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel new_pixel) {
    if (x < 0 || x >= (image->height) || y < 0 || y >= (image->width))
        return; 
    image->data[x * (image->width) + y] = new_pixel;
}

// input: image type of struct image, x - number of row (from 0 to height - 1), pixel_row - struct pixel_row - array of pixels - place where row will be save
struct pixel_row get_image_row(const struct image* image, uint64_t x) {
    struct pixel_row row = create_pixel_row(image->width);
    if (x < 0 || x >= (image->height))
        return row;
    row.size = image->width;
    for (uint64_t i = 0; i < row.size; i++) 
        row.data[i] = image->data[x * (image->width) + i];
    
    return row;
}

// input: image type of struct image, x - number of row (from 0 to height - 1), new_pixel_row - struct pixel_row - array of pixels
void set_image_row(struct image* image, uint64_t x, struct pixel_row* new_pixel_row) {
    if (x < 0 || x >= (image->height) || new_pixel_row->size != image->width)
        return; 
    for (uint64_t i = 0; i < new_pixel_row->size; i++) 
        image->data[x * (image->width) + i] = new_pixel_row->data[i];
}

// input: image type of struct image
void free_image_memory(struct image* image) {
    image->width = 0;
    image->height = 0;
    free(image->data);
}

void free_image_row_memory(struct pixel_row* pixel_row) {
    pixel_row->size = 0;
    free(pixel_row->data);
}

