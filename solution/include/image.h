#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel* data; 
};

struct pixel_row {
    uint64_t size;
    struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);

struct pixel_row create_pixel_row(uint64_t width);

struct pixel get_image_pixel(const struct image* image, uint64_t x, uint64_t y);

void set_image_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel new_pixel);

struct pixel_row get_image_row(const struct image* image, uint64_t x); 

void set_image_row(struct image* image, uint64_t x, struct pixel_row* new_pixel_row);

void free_image_memory(struct image* image);

void free_image_row_memory(struct pixel_row* pixel_row);

#endif
