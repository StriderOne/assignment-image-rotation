#include "bmp.h"
#include "file.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



int main(const int argc, char* const* argv)
{       
        if (argc != 3) {
                return 1;
        }
        char* input_filename = argv[1];
        char* output_filename = argv[2];
        struct image img = {0};


        FILE* input_file_ptr = NULL;

  
        FILE* output_file_ptr = NULL;
        if (open_file(input_filename, &input_file_ptr, "rb") == OPEN_OK) {
              
                from_bmp(input_file_ptr, &img);
                // return 0;
                if (close_file(input_file_ptr) != CLOSE_OK) 
                        return 1;
                struct image new_img = {0};
                new_img = image_rotate(&img);
                free_image_memory(&img);
                if (open_file(output_filename, &output_file_ptr, "wb") != OPEN_OK) 
                        return 1;
                to_bmp(output_file_ptr, &new_img);
                if (close_file(output_file_ptr) != CLOSE_OK)
                        return 1;
                free_image_memory(&new_img);
        } else {
                return 1;
        }

        return 0;
}
