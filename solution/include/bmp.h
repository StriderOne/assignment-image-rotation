#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "transformation.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum read_status  {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  SEEK_ERROR
};

enum read_status from_bmp(FILE * const file, struct image* img );

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  SEEK_PADDING_ERROR
};

enum write_status to_bmp( FILE* const file, struct image const * img);


#endif
