#ifndef TRNS_H
#define TRNS_H

#include "image.h"

struct image image_rotate(struct image* img);

#endif
