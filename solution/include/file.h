#ifndef FILE_H
#define FILE_H

#include <stdio.h>

enum open_status{
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status open_file(char* filename, FILE** file, char* actionTYPE);

enum close_status close_file(FILE* file);

#endif
