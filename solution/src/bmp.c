#include "bmp.h"

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

static uint32_t padding_count(uint32_t width, uint32_t bytes_per_pixel) {
        uint32_t padding = (uint32_t)(4 - ((width * bytes_per_pixel) % 4));
        padding %= 4;

        return padding;
}

static struct bmp_header create_header_template(struct image const* image_prototype) 
{
        const uint32_t bytes_per_pixel = (uint32_t) 24/8; 
        
        const uint32_t padding = padding_count(image_prototype->width, bytes_per_pixel);  //count of bytes in padding

        const struct bmp_header header_template = {
                .bfType = 19778,
                .biSizeImage = (bytes_per_pixel * image_prototype->width + padding) * image_prototype->height,
                .bfileSize = (bytes_per_pixel * image_prototype->width + padding) * image_prototype->height + sizeof(struct bmp_header),
                .biSize = 40,
                .biHeight = image_prototype->height,
                .biWidth = image_prototype->width,
                .biBitCount = 24,
        };

        return header_template;
}



enum read_status from_bmp(FILE * const file, struct image* img)
{

        struct bmp_header bmp = {0};
        
        //read file
        if (fread(&bmp, sizeof(struct bmp_header), 1, file) <= 0) return READ_INVALID_HEADER;

        // creating new image type of struct image
        struct image image = create_image(bmp.biWidth, bmp.biHeight);
        
        // calculating padding
        const uint32_t bytes_per_pixel = (uint32_t) bmp.biBitCount/8; 
        const uint32_t padding = padding_count(bmp.biWidth, bytes_per_pixel);  //count of bytes in padding
        const uint32_t unpaddedRowSize = bmp.biWidth*bytes_per_pixel;  //bites per valueble data at stroke, not including padding bytes
        const uint32_t paddedRowSize = unpaddedRowSize + padding;  //bites per all stroke, including padding bytes
      
        struct pixel_row currentRowPointer = create_pixel_row(bmp.biWidth);

        int64_t bmp_size = (int64_t)sizeof(struct bmp_header);
        
        for (uint32_t i = 0; i < bmp.biHeight; i++)
        {
                if (fseek(file, bmp_size+(i*paddedRowSize), SEEK_SET) != 0) {
                        free_image_memory(&image);
                        return SEEK_ERROR;
                }
                if (fread(currentRowPointer.data, sizeof(struct pixel), bmp.biWidth, file) <= 0) {
                        free_image_memory(&image);
                        return READ_INVALID_BITS; 
                }

                currentRowPointer.size = bmp.biWidth;
                set_image_row(&image, i, &currentRowPointer);
        }
        free_image_row_memory(&currentRowPointer);
        *img = image;
        
        return READ_OK;
}

enum write_status to_bmp(FILE * const file, struct image const* img) {

        const struct bmp_header new_bmp_header = create_header_template(img);
        
        if (fwrite(&new_bmp_header, sizeof(struct bmp_header), 1, file) <= 0) return WRITE_ERROR;

        for (uint32_t i = 0; i < img->height; i++) {

                struct pixel_row currentPixelRow = get_image_row(img, i);
                if (fwrite(currentPixelRow.data, sizeof(struct pixel), currentPixelRow.size, file) <= 0) return WRITE_ERROR;

                if (fseek(file, padding_count(img->width, 3), SEEK_CUR) != 0) return SEEK_PADDING_ERROR;
                free_image_row_memory(&currentPixelRow);
        }
        
        return WRITE_OK;

}
