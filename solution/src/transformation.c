#include "transformation.h"
#include <stdio.h>

struct image image_rotate(struct image* img) {
    struct image new_img = create_image(img->height, img->width);
    
    for (uint32_t i = 0; i < img->height; i++) {
        for (uint32_t j = 0; j < img->width; j++) {
                struct pixel currentPixel = get_image_pixel(img, i, j);
                set_image_pixel(&new_img, j, new_img.width - i - 1, currentPixel);      
        }
    }
    return new_img;
}
